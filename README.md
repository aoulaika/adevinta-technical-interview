# Usage
```
usage: app.py [-h] path

Words search

positional arguments:
  path        path to explore

optional arguments:
  -h, --help  show this help message and exit
```
# run the program (type :quit to exit)
Use the command bellow to run the file searcher
```
python app.py files_path
```
it will print something like this and invite you to type your queries
```
100 files read in directory files
search > 
```

# Unit test
to run unittest, use the command bellow
```
python -m unittest
```
# Some idea of improvments:

* add case sensitive check for sub phrase with a different weight
* add more unit tests
* parallelize search on files (this will add some race conditions complexity)
* parallelize search of each sub phrase in a file (this will add some race conditions complexity)