from argparse import ArgumentParser
from collections import Counter
import os


class Path:
    def __init__(self, fpath: str):
        self._fpath = fpath
        self._files = []
        self.__fill_files()

    def __fill_files(self):
        try:
            self._files = [
                os.path.join(self._fpath, f)
                for f in os.listdir(self._fpath)
                if os.path.isfile(os.path.join(self._fpath, f))
            ]
        except FileNotFoundError:
            pass

    def has_files(self):
        return len(self._files) > 0

    def __str__(self):
        return f"{len(self._files)} files read in directory {self._fpath}"


class Search:
    def __init__(self, files: str, words: list):
        self._files = files
        self._words = words
        self.__counter = Counter()
        self.run()

    def run(self):
        sub_phrases = self.sub_phrases

        for fpath in self._files:
            with open(fpath) as stream:
                content = stream.read()
                fname = fpath.split("/")[-1]

                if self._find(sub_phrases[-1], content, fname):
                    continue

                for phrase in sub_phrases[:-1]:
                    self._find(phrase, content, fname)

    def _find(self, phrase: str, content: str, fname: str):
        if phrase in content:
            self._update_counter(phrase, fname)
            return True
        return False

    def _update_counter(self, phrase: str, fname: str):
        phrase_weight = self.phrase_weight
        self.__counter.update({fname: phrase_weight[phrase]})

    @property
    def sub_phrases(self):
        """create a combination of possible sub phrases from the searched words,
        with no permutation

        Returns:
            list -- list of sub phrase
        """
        return [s for s in self._words] + [
            " ".join(self._words[:i]) for i in range(2, len(self._words) + 1)
        ]

    @property
    def total_weight(self):
        """the number of phrases to check
        
        Returns:
            int -- the number of phrases to check
        """
        return 2 * len(self._words) - 1

    @property
    def phrase_weight(self):
        """a percentage for each sub phrase, if our words to search are 'to' 'be'
        then 'to' weight 0.25 (25%) same as 'be'
        but 'to be' weight 1 (100%)
        
        Returns:
            dict -- phrase as key and their weight as value
        """
        sub_phrases_lists = [[s] for s in self._words] + [
            self._words[:i] for i in range(2, len(self._words) + 1)
        ]
        total_weight = self.total_weight
        return {
            **{" ".join(p): len(p) / total_weight for p in sub_phrases_lists[:-1]},
            " ".join(sub_phrases_lists[-1]): 1.0,
        }

    def top(self, n=None):
        for f, pc in self.__counter.most_common(n):
            print(f"{f}:{(pc*100):.2f}%")


if __name__ == "__main__":
    parser = ArgumentParser(description="Words search")
    parser.add_argument("path", type=str, help="path to explore")
    args = parser.parse_args()

    p = Path(fpath=args.path)
    if not p.has_files():
        print(f"{args.path} has no files")
        os.sys.exit(0)

    print(p)
    while True:
        inp = input("search > ")
        if inp == ":quit":
            break
        words = inp.split()
        s = Search(files=p._files, words=words)
        s.top(10)
