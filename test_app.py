from app import Search, Path
import unittest


class TestPath(unittest.TestCase):
    def test_path_files_len(self):
        path = Path("files")
        self.assertGreater(len(path._files), 0)

    def test_path_has_no_file(self):
        path = Path("some_path")
        self.assertEqual(len(path._files), 0)


class TestSearch(unittest.TestCase):
    def setUp(self):
        p = Path("files")
        self.s = Search(p._files, ["lorem", "ipsum"])

    def test_total_weight(self):
        self.assertEqual(self.s.total_weight, 3)

    def test_words_sub_phrases(self):
        self.assertListEqual(self.s.sub_phrases, ["lorem", "ipsum", "lorem ipsum"])


if __name__ == "__main__":
    unittest.main()
